/*jslint node: true */
var StaticServer = require('static-server');

var server = new StaticServer({
    rootPath: './public',
    port: 8080
});

server.start(function () {
    'use strict';
    console.log('Server started on port ' + server.port);
});