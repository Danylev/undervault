/*jslint node: true */
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var imagemin = require('imagemin');
var del = require('del');
var purge = require('gulp-css-purge');

/*Files paths*/
var SCRIPTS_PATH = './public/scripts/**/*.js';
var SCRIPT_END = '.public/scripts/lightbox.min.js'
var CSS_PATH = './public/css/**/*.css';
var UNIQUE_CSS = '.public/css/style.css';
var IMAGE_PATH = './public/img/*';
var FONTS_PATH = './public/font/*/**';
var DIST_PATH_CSS = './public/dist/css/';
var DIST_PATH_JS = './public/dist/js/';
var DIST_PATH_IMG = './public/dist/img/';
var DIST_PATH_FONTS = './public/dist/font/';
var HTML_PATH = './public/*.html';



/*_______*/
/*Styles*/
gulp.task('styles', ['clean:styles'], function () {
    "use strict";
    console.log('Concat and minifying styles');
    return gulp.src([CSS_PATH, UNIQUE_CSS])
        .pipe(plumber(function (someError) {
            console.log('Style Task Error');
            console.log(someError);
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(concat('all.min.css'))
		/*.pipe(purge({
	html: HTML_PATH,
	trim: false,
	shorten: true,
	verbose: false
}))*/
        .pipe(gulp.dest(DIST_PATH_CSS))
        .pipe(livereload());
});
/*Styles clean-up*/
gulp.task('clean:styles', function () {
    "use strict";
    console.log('Cleaning old styles');
    return del([DIST_PATH_CSS]);
});



/*________*/
/*Scripts*/
gulp.task('scripts', ['clean:scripts'], function () {
    "use strict";
    console.log('Starting working on scripts');
    return gulp.src(SCRIPTS_PATH)
        .pipe(uglify())
        .pipe(gulp.dest(DIST_PATH_JS))
        .pipe(livereload());
});
/*Scripts clean-up*/
gulp.task('clean:scripts', function () {
    "use strict";
    console.log('Cleaning old JS scripts');
    return del([DIST_PATH_JS]);
});



/*_______*/
/*Images*/
gulp.task('images', ['clean:images'], function () {
    "use strict";
    console.log('Minifying images and moving images');
    return gulp.src(IMAGE_PATH)
        .pipe(gulp.dest(DIST_PATH_IMG))
        .pipe(livereload());
});
/*Images clean-up*/
gulp.task('clean:images', function () {
    "use strict";
    console.log('Cleaning old images');
    return del([DIST_PATH_IMG]);
});


/*______*/
/*Fonts*/
gulp.task('fonts', ['clean:fonts'], function () {
    "use strict";
    console.log('Transfering fonts');
    return gulp.src(FONTS_PATH)
        .pipe(gulp.dest(DIST_PATH_FONTS))
        .pipe(livereload());
});
/*Fonts clean-up*/
gulp.task('clean:fonts', function () {
    "use strict";
    console.log('Deleting old fonts');
    return del([DIST_PATH_FONTS]);
});
/*_______*/
/*Default*/
gulp.task('default', ['images', 'styles', 'scripts', 'fonts'], function () {
    'use strict';
    console.log('Default task executed');

});

/*Watch*/
gulp.task('watch', function () {
    'use strict';
    console.log('Starting watching');
    require('./server.js');
    livereload.listen();
    gulp.watch([SCRIPTS_PATH, HTML_PATH], ['scripts']);
    gulp.watch([CSS_PATH, HTML_PATH], ['styles']);
    gulp.watch([HTML_PATH, IMAGE_PATH], ['images']);
    gulp.watch([HTML_PATH, FONTS_PATH], ['fonts']);
});
